sluggable-cyrillic это плагин для yii2-elfinder
===========================

Можно установить через Composer

php composer.phar require ldv/sluggable-cyrillic

Либо просто файл SluggableCyr.php положить в папку 
\vendor\mihaildev\yii2-elfinder\plugin
(ручное подключение без composer не проверялось)

Использовать согласно инструкции по подключении плагинов к elfinder
            'plugin' => [
                [
                   // 'class'=>'\mihaildev\elfinder\plugin\Sluggable',
                     'class'=>'\mihaildev\elfinder\plugin\Sluggable\SluggableCyr',
                    'lowercase' => true,
                    'replacement' => '-',
                 //   'cyrsupport' => true
                ]
            ],
